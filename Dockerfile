FROM alpine:3.6

LABEL mainteiner="Patricio Perpetua <patricio.perpetua.arg@gmail.com>" \
    name="singletonar/parallella-dev" \
    architecture="x86_64" \
    vendor="SINGLETON" \
    vcs-type="git" \
    vcs-url="https://gitlab.com/singleton.com.ar/esp8266-docker.git" \
    distribution-scope="public" \
    Summary="Image to program and compile binaries for ESP8266."

ENV LANG LC_CTYPE en_US.UTF-8
ENV HOME /home/netbeans

# Dowloading and installing necessaries packages...
RUN adduser -D arduino && \
    apk add --no-cache --update \
    # Equivalent to build-essentials
    alpine-sdk \
    curl \
    ca-certificates \
    wget \
    git \
    # Libraries to user interface to the container
    libxext \
    libxrender \
    libxtst \
    ttf-dejavu \
    # End of libraries.
    python \
    py-pip \
    picocom \
    tar \
    # Arduino libs.
    gcc-avr \
    avr-libc \
    avrdude && \
    rm -rf /tmp/* && \
    rm -rf /var/cache/apk/*

# Java Version
ENV JAVA_VERSION_MAJOR 8
ENV JAVA_VERSION_MINOR 45
ENV JAVA_VERSION_BUILD 14
ENV JAVA_PACKAGE       jdk

# Download and unarchive Java
RUN mkdir /opt && curl -jksSLH "Cookie: oraclelicense=accept-securebackup-cookie"\
  http://download.oracle.com/otn-pub/java/jdk/${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-b${JAVA_VERSION_BUILD}/${JAVA_PACKAGE}-${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-linux-x64.tar.gz \
    | tar -xzf - -C /opt &&\
    ln -s /opt/jdk1.${JAVA_VERSION_MAJOR}.0_${JAVA_VERSION_MINOR} /opt/jdk &&\
    rm -rf /opt/jdk/*src.zip \
           /opt/jdk/lib/missioncontrol \
           /opt/jdk/lib/visualvm \
           /opt/jdk/lib/*javafx* \
           /opt/jdk/jre/lib/plugin.jar \
           /opt/jdk/jre/lib/ext/jfxrt.jar \
           /opt/jdk/jre/bin/javaws \
           /opt/jdk/jre/lib/javaws.jar \
           /opt/jdk/jre/lib/desktop \
           /opt/jdk/jre/plugin \
           /opt/jdk/jre/lib/deploy* \
           /opt/jdk/jre/lib/*javafx* \
           /opt/jdk/jre/lib/*jfx* \
           /opt/jdk/jre/lib/amd64/libdecora_sse.so \
           /opt/jdk/jre/lib/amd64/libprism_*.so \
           /opt/jdk/jre/lib/amd64/libfxplugins.so \
           /opt/jdk/jre/lib/amd64/libglass.so \
           /opt/jdk/jre/lib/amd64/libgstreamer-lite.so \
           /opt/jdk/jre/lib/amd64/libjavafx*.so \
           /opt/jdk/jre/lib/amd64/libjfx*.so

# Set environment
ENV JAVA_HOME /opt/jdk
ENV PATH ${PATH}:${JAVA_HOME}/bin

#     arduino-core && \
RUN pip install ino

# Dowload Arduino IDE 
# https://downloads.arduino.cc/arduino-1.8.5-linux32.tar.xz

# Creating volumes...
# VOLUME /src



# COPY scripts/main.sh /

# VOLUME /data
# VOLUME ~/.netbeans
# VOLUME ~/NetBeansProjects
# VOLUME /usr/lib/node_modules
# VOLUME ~/.npm
# VOLUME ~/.cache/netbeans
# VOLUME $HOME/netbeans/java/maven/conf


# USER arduino
# WORKDIR /src

# WORKDIR /data
# EXPOSE 8000 9000

# CMD ~/netbeans/bin/netbeans
# ENTRYPOINT ["/main.sh"]
# CMD ["default"]