# esp-dev

----------------------

## Development environment for  [ESP WIFI boards](https://www.espressif.com/sites/default/files/documentation/0a-esp8266ex_datasheet_en.pdf)

## Installing and running

----------------------

Start by [installing docker](https://docs.docker.com/installation/#installation) for your platform. There is un script available here called install_docker.sh to install docker on a computer.

The rest of this guide is based on a alpine environment, but any other environment should be similar.

This container is based on a alpine 3.6 image. You can start the container by executing run.sh and providing a folder to attach the workspace folder of the container where code will be written:

```bash
 ./run.sh -p workspace
```

```bash
 ./run.sh -p /home/my/computer/full/path/workspace
```

This will create a BASH shell in the `$HOME` directory which is located at `/home/dev/`.

There are three directories you need to know about:

TODO

## Compiling code

----------------------

```bash
```

```bash
```
PUT GIF HERE TO SHOW

## Running UI

----------------------

```bash
```

```bash
```
PUT GIF HERE TO SHOW

## Related Documents

----------------------

* [Arduino CORE](https://github.com/esp8266/Arduino)
* [ESP Open SDK](https://github.com/pfalcon/esp-open-sdk)

* [NodeMCU Lua Firmware for ESP8266](https://github.com/nodemcu/nodemcu-firmware)

* [ESP8266 - ESP8266 - Easiest way to program so far (Using Arduino IDE)](http://www.whatimade.today/esp8266-easiest-way-to-program-so-far/)

* [Arduino Development Environment for Docker](https://hub.docker.com/r/strm/dev-arduino/)

* [Build Environment of espressif's Wi-Fi(or bluetooth) Soc](https://hub.docker.com/r/vowstar/esp8266/)

* [Arduino IDE Installer](https://www.arduino.cc/en/Guide/Linux)

* [ESP8266 Arduino Core GitHub](http://esp8266.github.io/Arduino/versions/2.0.0/doc/installing.html)

## Contributing

----------------------

Contributions to this repository are very welcome.

To contribute, please fork this repository on GitLab and send a pull request with a clear description of your changes. If appropriate, please ensure that the user documentation in this README is updated.

If you have submitted a PR and not received any feedback for a while, feel free to [ping me on Twitter](https://twitter.com/patoperpetua)

## TODO

----------------------

* [ ] Finish README file.
* [ ] Put a link to download only the run script and docker script.
* [ ] Create test scripts.
* [ ] Provide a full example to use console and UI image.

----------------------

© [Perpetua Patricio](http://patricioperpetua.singleton.com.ar), [SINGLETON Software Solutions](http://www.singleton.com.ar/), Argentina, 2018.
